# Bomba - Python

Si no presionas "toc" explota.

## Descripción:

Crea un objeto temporizador, el cual periódicamente hay que enviarle una señal.
Si el tiempo expira y no se recibe la señal entonces ejecutará un programa.


## Ejemplo de uso:


    def vigilador ():
        mensaje = robot.enviar_mensaje (JAVIER, '🏓') # Envía un mensaje al más lindo de la sala.
        if mensaje is not None:                       # Si el mensaje fue recibido:
            robot.borrar_mensaje (mensaje)            # Elimina el mensaje (no llena el privado)
            return True                               # Impide la detonación por 60 segundos.
        return False                                  # Permite que explote la bomba


    # Crea una bomba
    bomba = Bomba (tiempo=60, vigilante=vigilador)

    # Límite de tolerancia que permitirá que expire el temporizador.
    bomba.limite = 2

    # Activa la bomba
    bomba.activar ()

    # Impide que explore
    bomba.toc ()

    # Desactiva la bomba por completo.
    bomba.desactivar ()


El programa usa el algoritmo de vigilancia esperando a que devuelva verdadero.
En caso contrario, si expira (60 s) y supera el límite permitido (2), activa el algoritmo "explosivo".


## Más proyectos:
Date una vuelta por https://gitlab.com/javipc/mas

Gracias por la visita 👋🏿
