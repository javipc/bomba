#!/bin/python3

import threading
import os
import sys
import time
import __main__

"""
    Javier Martinez.

    Bomba: si no presionas "toc" explota.

    def vigilador ():    
        mensaje = robot.enviar_mensaje (JAVIER, '🏓') # Envía un mensaje al más lindo de la sala.
        if mensaje is not None:                       # Si el mensaje fue recibido:
            robot.borrar_mensaje (mensaje)            # Elimina el mensaje (no llena el privado)
            return True                               # Impide la detonación por 60 segundos.
        return False                                  # Permite que explote la bomba

    
    # Crea una bomba
    bomba = Bomba (tiempo=60, vigilante=vigilador)

    bomba.limite = 2

    # Activa la bomba
    bomba.activar ()

    # Impide que explore
    bomba.toc ()

    # Desactiva la bomba por completo.
    bomba.desactivar ()

    
    El programa usa el algoritmo de vigilancia esperando a que devuelva verdadero.
    En caso contrario, si expira (60 s) y supera el límite permitido (2), activa el algoritmo "explosivo".
    

    
    https://stackoverflow.com/questions/11329917/restart-python-script-from-within-itself#11329970

"""


class Bomba :

    """
        @param tiempo: entero, valor del intervalo en el que repetira la comprobación.        
        @param explosivo: funcion, es lo que ejecutará en caso de no cumplir con la señal "toc" a tiempo.
        @param vigilante: funcion, es una función que debe devolver verdadero.
    """

    def __init__ (self, tiempo = 60, explosivo = None, vigilante=None):
        # Intervalo de tiempo, repetirá la comprobación en este período.
        self.tiempo = tiempo

        # Cantidad de comprobaciones que debe realizar antes de detonarse.
        self.limite = 2
        
        # Uso interno
        self.listo_para_explotar = self.limite
        
        # Uso interno
        self.listo_para_desconectar = False

        # Muestra mensajes
        self.tic = False

        # Función a ejecutar en caso de que termine el tiempo.
        self.explosivo = explosivo
        if self.explosivo is None:
            self.explosivo = self.programa_reiniciar

        # Función que debería enviar un "toc" para evitar la ejecución de explosivo.
        self.vigilante = vigilante

        # Explosivo asíncrono.
        self.asincrono = False

    def toc (self):
        self.listo_para_explotar = self.limite
        if self.tic:
            print ( 'Bomba: toc' )
        

    def desactivar (self):
        self.listo_para_explotar = self.limite +1
        self.listo_para_desconectar = True
        
        print ('Bomba: Desactivando...')

    def empezar (self):        
        self.listo_para_desconectar = False
        while not self.listo_para_desconectar:
            time.sleep (self.tiempo)
            if self.listo_para_explotar < 0:
                self.explosivo ()
                break
            self.listo_para_explotar = self.listo_para_explotar -1
            if self.tic:
                print ('Bomba: tic', self.listo_para_explotar)
            

        self.desactivar ()
        print ('Bomba: ¡Temporizador terminado!')
        
    
    def activar (self):
        self.listo_para_explotar = self.limite
        self.hilo = threading.Thread (target=self.empezar, daemon=True)

        if self.vigilante is not None:
            def rutina_vigilancia ():
                while not self.listo_para_desconectar:
                    time.sleep (self.tiempo)
                    if self.vigilante () == True:
                        self.toc ()
                        
            self.hilo_vigilancia = threading.Thread (target=rutina_vigilancia, daemon= True)
            self.hilo_vigilancia.start ()

        self.hilo.start ()
        print ('Bomba: Activada.')

    def programa_reiniciar (self):
        self.listo_para_desconectar = True
        print ('¡BOOM!')
        print (sys.executable, [__main__.__file__])  
        os.execl(sys.executable, sys.executable, __main__.__file__)
        print ('¡Adios!')


